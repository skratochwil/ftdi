#include <stdio.h>
#include <ftd2xx.h>
#include <getopt.h>
#include <stdlib.h>

static int verbose_flag = 0;

/**
 * Set the driver's VID and PID, so it loads for devices with these IDs.
 */
static void adjust_driver_vid_pid(DWORD vid, DWORD pid) {
    FT_STATUS status;
    DWORD curr_vid, curr_pid;

    status = FT_GetVIDPID(&curr_vid, &curr_pid);
    if (status != FT_OK) {
        if (verbose_flag)
            printf("Error: FT_GetVIDPID() returned %#08x.\n", status);
    } else {
        if (verbose_flag)
            printf("Device reports: VID=%#04x, PID=%#04x\n", curr_vid, curr_pid);
    }

    if ( (curr_vid != vid) || (curr_pid != pid) ) {
        if (verbose_flag) {
            printf("Incompatible VID/PID combination. Reconfiguring driver.\n");
            printf("Setting values: VID=%#04x, PID=%#04x\n", vid, pid);
        }
        status = FT_SetVIDPID(vid, pid);

        if (status != FT_OK) {
            if (verbose_flag)
                printf("Error: FT_SetVIDPID() returnes %#08x.\n", status);
        }
    }
}

/**
 * Returns the handle of the first FT232 device found.
 */
static FT_HANDLE setup(void) {
    FT_STATUS status;
    LPDWORD numDevs;
    FT_HANDLE handle;

    status = FT_CreateDeviceInfoList(&numDevs);
    if (status != FT_OK) {
        if (verbose_flag)
            printf("Error: FT_CreateDeviceInfoList() returned %#08x.\n");
    } else {
        if (verbose_flag)
            printf("numDevs: %d\n", numDevs);
    }

    status = FT_Open(0, &handle);
    if (status != FT_OK) {
        if (verbose_flag)
            printf("Error: FT_Open() returned %d\n", status);
        printf("Could not open device. The reason might be a loaded ftdi_sio\n");
        printf("kernel module. Use 'lsmod | grep ftdi_sio' to check whether\n");
        printf("this module is loaded. If so, remove it using 'rmmod ftdi_sio'.\n");
        return NULL;
    } else {
        if (verbose_flag)
            printf("handle: %#016x.\n", handle);
        return handle;
    }
}


static void print_ft_program_data(PFT_PROGRAM_DATA data) {
    printf("EEPROM contents:\n");
    printf("\tVendorId: %#x\n", data->VendorId);
    printf("\tDeviceID: %#x\n", data->ProductId);
    printf("\tManufacturer: %s\n", data->Manufacturer);
    printf("\tManufacturerId: %s\n", data->ManufacturerId);
    printf("\tDescription: %s\n", data->Description);
    printf("\tSerialNumber: %s\n", data->SerialNumber);
}


/**
 * List EEPROM data.
 */
static void enumerate_eeprom(FT_HANDLE handle) {
    FT_STATUS status;
    FT_PROGRAM_DATA eepromData;
    char manufacturerBuf[32];
    char manufacturerIdBuf[16];
    char descriptionBuf[64];
    char serialNumberBuf[16];

    if (handle == NULL)
        return;

    eepromData.Signature1 = 0x00000000;
    eepromData.Signature2 = 0xFFFFFFFF;
    eepromData.Version = 2;
    eepromData.Manufacturer = manufacturerBuf;
    eepromData.ManufacturerId = manufacturerIdBuf;
    eepromData.Description = descriptionBuf;
    eepromData.SerialNumber = serialNumberBuf;

    status = FT_EE_Read(handle, &eepromData);
    if (status != FT_OK) {
        printf("Error: FT_EE_Read() returned %d\n", status);
        return;
    } else {
        print_ft_program_data(&eepromData);
    }
}


static void write_eeprom(FT_HANDLE handle, WORD vid, WORD pid) {
    FT_STATUS status;
    FT_PROGRAM_DATA eepromData;
    char manufacturerBuf[32];
    char manufacturerIdBuf[16];
    char descriptionBuf[64];
    char serialNumberBuf[16];

    if (handle == NULL)
        return;

    eepromData.Signature1 = 0x00000000;
    eepromData.Signature2 = 0xFFFFFFFF;
    eepromData.Version = 2;
    eepromData.Manufacturer = manufacturerBuf;
    eepromData.ManufacturerId = manufacturerIdBuf;
    eepromData.Description = descriptionBuf;
    eepromData.SerialNumber = serialNumberBuf;

    if (verbose_flag)
        printf("Reading device configuration...\n");

    status = FT_EE_Read(handle, &eepromData);
    if (status != FT_OK) {
        printf("Error: FT_EE_Read() returned %d\n", status);
    }

    if (verbose_flag)
        printf("Applying new configuration data...\n");

    if (eepromData.VendorId != vid)     eepromData.VendorId = vid;
    if (eepromData.ProductId != pid)    eepromData.ProductId = pid;

    if (verbose_flag)
        printf("Writing configuration to device...\n");

    
    status = FT_EE_Program(handle, &eepromData);
    if (status != FT_OK) {
        printf("Error: FT_EE_Program() returned %d\n", status);
    } else {
        printf("Success.");
    }
}


void print_help() {
    printf("Usage:\n");
    printf("\t-r --read           \tdisplay configuration stored in EEPROM\n");
    printf("\t-v --write-vid [vid]\tset new vendor id (hex or decimal)\n");
    printf("\t-d --write-pid [pid]\tset new device id (hex or decimal)\n");
    printf("\t-a --driver-vid [vid]\tset driver vendor id (hex or decimal)\n");
    printf("\t-b --driver-pid [pid]\tsetdriver product id (hex or decimal)\n");
    printf("\t   --verbose        \tmore output\n");
}

typedef struct _PIDVID {
    DWORD vid;
    DWORD pid;
    int dirty;
} PIDVID;

int main(int argc, char** argv) {
    int c;

    PIDVID newSettings      = { 0x0000, 0x0000, 0};
    PIDVID driverSettings   = { 0x0403, 0x6001, 0};
    FT_HANDLE handle;

    while(1) {
        static struct option long_options[] = {
            {"verbose",     no_argument,        &verbose_flag,  1},
            {"read",        no_argument,        0,              'r'},
            {"driver-vid",  required_argument,  0,              'a'},
            {"driver-pid",  required_argument,  0,              'b'},
            {"write-vid",   required_argument,  0,              'v'},
            {"write-pid",   required_argument,  0,              'p'},
            {0, 0, 0, 0}
        };

        int option_index = 0;

        c = getopt_long(argc, argv, "ra:b:v:p:", long_options, &option_index);

        if (c == -1)
            break;

        switch(c) {
            case 0:
                if (verbose_flag) {
                    printf("verbose on.\n");
                }
                break;

            case 'r':
                // triggers read and enumerate operations
                driverSettings.dirty = 1;
                break;

            case 'a':
                driverSettings.vid = strtol(optarg, NULL, 0);
                printf("Read %s. Will set the driver VID to %#04x\n", optarg, driverSettings.vid);
                driverSettings.dirty = 1;
                break;

            case 'b':
                driverSettings.pid = strtol(optarg, NULL, 0);
                printf("Read %s. Will set the driver PID to %#04x\n", optarg, driverSettings.pid);
                driverSettings.dirty = 1;
                break;

            case 'v':
                newSettings.vid = strtol(optarg, NULL, 0);
                printf("Read %s. Will set the new VID to %#04x\n", optarg, newSettings.vid);
                newSettings.dirty = 1;
                break;

            case 'p':
                newSettings.pid = strtol(optarg, NULL, 0);
                printf("Read %s. Will set the new PID to %#04x\n", optarg, newSettings.pid);
                newSettings.dirty = 1;
                break;

            default:
                print_help();
                printf("Exiting.\n");
                return 1;
                break;
            
        }
    }

    if (verbose_flag) {
        printf("\tDEBUG -- driverSettings: vid=%#04x, pid=%#04x, dirty=%d\n", 
                driverSettings.vid, driverSettings.pid, driverSettings.dirty);
        printf("\tDEBUG -- newSettings: vid=%#04x, pid=%#04x, dirty=%d\n", 
                newSettings.vid, newSettings.pid, newSettings.dirty);
    }

    printf("Processing input...\n");

    if (driverSettings.dirty)
       adjust_driver_vid_pid(driverSettings.vid, driverSettings.pid); 

    handle = setup();
    if (handle == NULL) {
        printf("Error: cannot get handle for device. Exit.\n");
        return 1;
    }

    // yeah, its ugly to use a dirty flag for a "print me!!" notification...
    if (driverSettings.dirty) {
        enumerate_eeprom(handle);
    }

    if (newSettings.dirty) {
        char selection;
        printf("Are you sure? [y/n] ");
        scanf(" %c", &selection);

        if (selection == 'y') {
            write_eeprom(handle, newSettings.vid, newSettings.pid);
        } else {
            printf("Exit.\n");
        }
    }
}
